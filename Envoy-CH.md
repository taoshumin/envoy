# Envoy

### 服务网格的基本功能

![img.png](img.png)

### 服务网格的部署模式

![img_1.png](img_1.png)

### Envoy几个显著的特性

![img_2.png](img_2.png)


### Envoy基本组件

- 常用设计架构

![img_3.png](img_3.png)

- 基本组件
  ![img_4.png](img_4.png)

- 常用术语
  ![img_5.png](img_5.png)
