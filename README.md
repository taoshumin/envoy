# 视频

- [youtu.be](https://youtu.be/40gKzHQWgP0)
- [https://www.envoyproxy.io/docs/envoy/latest/intro/arch_overview/upstream/connection_pooling#arch-overview-conn-pool](https://www.envoyproxy.io/docs/envoy/latest/intro/arch_overview/upstream/connection_pooling#arch-overview-conn-pool)
- [https://www.envoyproxy.io/docs/envoy/latest/intro/arch_overview/intro/terminology](https://www.envoyproxy.io/docs/envoy/latest/intro/arch_overview/intro/terminology)

# Install

```shell
arch -arm64 brew install envoy
```

# certbot

- [https://certbot.eff.org/](https://certbot.eff.org/)

```shell
arch -arm64 brew install certbot
```

#### 生成证书

```shell
sudo certbot certonly --standalone
```


# 架构解释

- [Envoy-Youtube.md](Envoy.md)
- [Envoy-CH.md](Envoy-CH.md)



### 常用部署方案

![img_6.png](img_6.png)

### 部署使用

- envoy 部署

```shell
docker run --name=envoy -d \
  -p 80:10000 \
  -v $(pwd)/http.yaml:/etc/envoy/http.yaml \
  envoyproxy/envoy:latest
```

- envoy 管理界面

```shell
docker run --name=envoy-with-admin -d \
    -p 9901:9901 \
    -p 10000:10000 \
    -v $(pwd)/http.yaml:/etc/envoy/http.yaml \
    envoyproxy/envoy:latest
```
